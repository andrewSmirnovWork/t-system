package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Stack;
import java.util.StringTokenizer;

public class Calculator {


    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private static String delimetr = "()+-*/ ,";
    private static String operator = "+-*/";
    private static String bracket = "()";

    private static boolean isDelimetr(String input){
        for(int i = 0; i < delimetr.length(); i++){
            if(input.charAt(0) == delimetr.charAt(i)) return true;
        }
        return false;
    }
    private static boolean isSeparator(String input){
        return input.equals(",");
    }

    private static boolean isBracket(String input){
        for(int i = 0; i < bracket.length(); i++){
            if(input.charAt(0) == bracket.charAt(i)) return true;
        }
        return false;
    }

    private static boolean isOperator(String input){
        for(int i = 0; i < operator.length(); i++){
            if(input.charAt(0) == operator.charAt(i)) return true;
        }
        return false;
    }
    private static String CorrectNumber(String token) {
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);
        try{
            return df.format(Double.parseDouble(token));
        }
        catch (Exception e){
            return null;
        }
        /*for (Number n : Arrays.asList(12, 123.12345, 0.23, 0.1, 2341234.212431324)) {
            Double d = n.doubleValue();
            System.out.println(df.format(d));
        }*/
    }
    private static int priority(String input) {
        switch (input)
        {
            case "(": return 0;
            case ")": return 1;
            case "+": return 2;
            case "-": return 3;
            case "*": return 4;
            case "/": return 4;
            default: return 5;
        }
    }

    static String toRPN(String input){
        StringTokenizer token = new StringTokenizer(input, delimetr, true);
        Stack<String> stack = new Stack<>();
        StringBuilder RPN = new StringBuilder();
        while(token.hasMoreTokens()) {
            String strToken = token.nextToken();
            if(strToken.equals(" ")) continue;
            if(isSeparator(strToken))return null;
            if (isDelimetr(strToken)) {
                if(!stack.isEmpty() && stack.peek().equals(strToken) && !isBracket(strToken)) return null;
                if (strToken.equals("(")) stack.push(strToken);
                else if (strToken.equals(")")) {
                    while (!stack.peek().equals("(")) {
                        RPN.append(stack.pop()).append(" ");
                        if (stack.isEmpty()) {
                            System.out.println(RPN.toString());
                            System.out.println("Wrong expression, Bracket is not open");
                            return null;
                        }
                    }
                    stack.pop();
                }
                else if(!stack.isEmpty())
                {
                    if (priority(strToken) > priority(stack.peek())) {
                        stack.push(strToken);
                    } else {
                        RPN.append(stack.pop()).append(" ");
                        stack.push(strToken);
                    }
                }
                else stack.add(strToken);
            } else RPN.append(CorrectNumber(strToken)).append(" ");
        }
        while(!stack.isEmpty()){
            if(isOperator(stack.peek())) RPN.append(stack.pop()).append(" ");
            else {
                System.out.println("Bracket is not closed");
                return null;
            }
        }
        //if(isOperator(RPN.get(RPN.size()-1))) System.out.println("Wrong expression");
        System.out.println(RPN);
        return RPN.toString();
    }

    static String counting(String RPN){

       /* DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);
        for (Number n : Arrays.asList(12, 123.12345, 0.23, 0.1, 2341234.212431324)) {
            Double d = n.doubleValue();
            System.out.println(df.format(d));
        }*/
        if(RPN == null)return null;
        Stack<Double> stack = new Stack<>();
        StringTokenizer token = new StringTokenizer(RPN, " ",false );
        while(token.hasMoreTokens()){
            String strToken = token.nextToken();
            switch (strToken) {
                case "-": {
                    Double b = stack.pop();
                    Double a = stack.pop();
                    stack.push(a - b);
                    break;
                }
                case "+":
                    stack.push(stack.pop() + stack.pop());
                    break;
                case "*":
                    stack.push(stack.pop() * stack.pop());
                    break;
                case "/": {
                    Double b = stack.pop();
                    Double a = stack.pop();
                    stack.push(a / b);
                    break;
                }
                default:
                    stack.push(Double.valueOf(strToken));
                    break;
            }
        }
        return stack.pop().toString();
    }


    public String evaluate(String statement){
        if(statement!=null&& !statement.equals("")) {
        try {
            String answer = counting(toRPN(statement));

            int index = 0;
            if (answer != null) {
                index = answer.indexOf(".") + 1;
                if ((answer.charAt(index) == '0') && (answer.length() == index + 1)) {
                    answer = answer.substring(0, index - 1);
                    return answer;
                }
                BigDecimal bd = new BigDecimal(answer);
                bd = bd.setScale(4, RoundingMode.HALF_UP);
                return Double.toString(bd.doubleValue());
            }
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
        return null;
    }
}
