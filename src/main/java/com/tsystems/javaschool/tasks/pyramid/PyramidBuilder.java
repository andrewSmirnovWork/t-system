package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        //List<Integer> sortedInput = new ArrayList<>();


        if(inputNumbers.contains(null)) throw new CannotBuildPyramidException();

        int rows = rowsNumber(inputNumbers);
        if(rows == -1) throw new CannotBuildPyramidException();
        int colums = 2 * rows - 1;
        int[][] pyramid = new int[rows][colums];

        Collections.sort(inputNumbers);
        LinkedList<Integer> queue = new LinkedList<>(inputNumbers);
        int startPosition = (pyramid[0].length) / 2;
        for(int i = 0; i < pyramid.length; i++){
                int start = startPosition;
            for(int j = 0; j <= i; j++ ){
                pyramid[i][start] = queue.remove();
                start += 2;
            }
            startPosition --;
        }
        return pyramid;
    }

    public static int rowsNumber(List<Integer> listInput) {
        int size = listInput.size();
        double result = ((Math.sqrt(1 + 8 * size))-1) / 2;
        if(result == Math.ceil(result)) return (int)result;
        return -1;
    }
}
